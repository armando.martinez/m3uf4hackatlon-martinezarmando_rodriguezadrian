﻿using System;
using System.Collections.Generic;
using M3UF4Hackatlon_MartinezArmando_RodriguezAdrian;

namespace M3UF4Hackatlon_MartinezArmando_RodriguezAdrian
{
    public class Tesoro: IRecolectable
    {
        public string Nombre { get; set; }
        public int Valor { get; set; }
        
        public Tesoro(string nombre, int valor)
        {
            this.Nombre = nombre;
            this.Valor = valor;
        }

        public Tesoro()
        {
            this.Nombre = "Tesoro";
            this.Valor = 0;
        }

        public void Recoger()
        {
            Console.WriteLine($"Nombre: {this.Nombre}\nValor: {this.Valor}");
        }
    }

    public abstract class Personaje: IAtacable
    {
        public string Nombre { get; set; }
        public int Vida { get; set; }
        public int Ataque { get; set; }

        public Personaje(string nombre, int vida, int ataque)
        {
            this.Nombre = nombre;
            this.Vida = vida;
            this.Ataque = ataque;
        }
        public Personaje()
        {
            this.Nombre = "Adrian";
            this.Vida = 10000;
            this.Ataque = 250;
        }
        public bool Morir()
        {
            Console.WriteLine($"{this.Nombre} ha MUERTO");
            return true;
        }

        public void RecibirDamage(int damage)
        {
            this.Vida -= damage;
            if (this.Vida <= 0) { Morir(); }
        }

        public void Atacar(IAtacable enemigo)
        {
            enemigo.RecibirDamage(this.Ataque);
        }
    }

    public class Jugador: Personaje
    {
        List<IRecolectable> Inventario { get; set; }

        public Jugador(string nombre, int vida, int ataque)
        {
            this.Nombre = nombre;
            this.Vida = vida;
            this.Ataque = ataque;
            this.Inventario = new List<IRecolectable>();
        }
        public Jugador()
        {
            this.Nombre = "Prota";
            this.Vida = 1;
            this.Ataque = 2;
            this.Inventario = new List<IRecolectable>();
        }

        public bool Moverse(string direccion)
        {
            switch (direccion)
            {
                case "arriba":
                case "abajo":
                case "derecha":
                case "izquierda":
                    Console.WriteLine(direccion);
                    return true;
                default:
                    Console.WriteLine("ERROR: DIRECCIÓN INCORRECTA");
                    return false;
            }
        }

        public void RecogerTesoro(IRecolectable obj)
        {
            this.Inventario.Add(obj);
            obj.Recoger();
        }
        public void MostrarInventario()
        {
            foreach(IRecolectable obj in this.Inventario)
            {
                Console.WriteLine();
                obj.Recoger();
            }
        }
    }

    public class Enemigo: Personaje
    {
        Tesoro Recompensa { get; set; }

        public Enemigo(string nombre, int vida, int ataque, Tesoro recompensa)
        {
            this.Nombre = nombre;
            this.Vida = vida;
            this.Ataque = ataque;
            this.Recompensa = recompensa;
        }
        public Enemigo()
        {
            this.Nombre = "Armando Estragos";
            this.Vida = 200;
            this.Ataque = 10000;
            this.Recompensa = new Tesoro();
        }

        public Tesoro Drop()
        {
            return this.Recompensa;
        }

    }

    public class Empty
    {
        public Empty() { }
    }
    class Juego
    {
        static void Main()
        {
            Jugador protagonista = new Jugador("Adrian", 1000, 400);
            Enemigo koopa = new Enemigo("Koopa", 300, 400, new Tesoro("Caparazón", 20));
            Enemigo troopa = new Enemigo("Troopa", 600, 100, new Tesoro("Caparazón azul", 60));

            object[,] map = new object[,]
            {
                {new Empty(), koopa, new Empty(), new Empty(), new Empty()},
                {new Empty(), new Empty(), new Empty(), new Empty(), troopa},
                {new Tesoro("Flor de fuego", 4000), new Empty(), new Empty(), new Empty(), new Empty()},
                {new Empty(), new Empty(), new Empty(), new Tesoro("Seta", 2000), new Empty()},
                {new Empty(), new Empty(), protagonista, new Empty(), new Empty()}
            };
            int numEnemigos = 2;
            int[] jugadorPos = new int[] { 4, 2 };
            int[] newPos = new int[] { 4, 2 };
            do
            {
                VerMapa(map);
                Console.WriteLine("Hacia donde quieres moverte?");
                string command = Console.ReadLine();
                Console.Clear();
                if (protagonista.Moverse(command))
                {
                    switch (command)
                    {
                        case "arriba":
                            newPos[0] -= 1;
                            break;
                        case "abajo":
                            newPos[0] += 1;
                            break;
                        case "derecha":
                            newPos[1] += 1;
                            break;
                        case "izquierda":
                            newPos[1] -= 1;
                            break;
                    }
                    switch (map[newPos[0], newPos[1]].ToString())
                    {
                        case "M3UF4Hackatlon_MartinezArmando_RodriguezAdrian.Enemigo":
                            Comparar(protagonista, (Enemigo)map[newPos[0], newPos[1]]);
                            Console.WriteLine("1. Atacar");
                            Console.WriteLine("2. Huir");
                            Console.WriteLine("Que opcion quieres?");
                            bool correct;
                            do
                            {
                                switch (Console.ReadLine())
                                {
                                    case "1":
                                        Enemigo enemy = (Enemigo)map[newPos[0], newPos[1]];
                                        correct = true;
                                        do
                                        {
                                            protagonista.Atacar((Enemigo)map[newPos[0], newPos[1]]);
                                            enemy.Atacar(protagonista);
                                        } while (protagonista.Vida > 0 && enemy.Vida > 0);
                                        if (protagonista.Vida > 0) { protagonista.RecogerTesoro(enemy.Drop()); }
                                        map[newPos[0], newPos[1]] = protagonista;
                                        map[jugadorPos[0], jugadorPos[1]] = new Empty();
                                        jugadorPos = new int[] { newPos[0], newPos[1] };
                                        numEnemigos--;
                                        break;
                                    case "2":
                                        correct = true;
                                        Console.WriteLine("Vuelves a la casilla anterior");
                                        newPos = new int[] { jugadorPos[0], jugadorPos[1] };
                                        break;
                                    default:
                                        correct = false;
                                        break;
                                }
                            } while (!correct);
                            break;
                        case "M3UF4Hackatlon_MartinezArmando_RodriguezAdrian.Tesoro":
                            Tesoro tesoro = (Tesoro)map[newPos[0], newPos[1]];
                            protagonista.RecogerTesoro(tesoro);
                            map[newPos[0], newPos[1]] = protagonista;
                            map[jugadorPos[0], jugadorPos[1]] = new Empty();
                            jugadorPos = new int[]{ newPos[0], newPos[1]};
                            break;
                        case "M3UF4Hackatlon_MartinezArmando_RodriguezAdrian.Empty":
                            map[newPos[0], newPos[1]] = protagonista;
                            map[jugadorPos[0], jugadorPos[1]] = new Empty();
                            jugadorPos = new int[]{ newPos[0], newPos[1] };
                            break;
                    }
                }
            } while (protagonista.Vida > 0 && numEnemigos > 0);
        }

        static void Comparar(IAtacable jugador, IAtacable enemigo)
        {
            Personaje pJugador = (Personaje)jugador;
            Personaje pEnemigo = (Personaje)enemigo;
            if (pJugador.Vida > pEnemigo.Vida) { Console.WriteLine($"La vida de {pJugador.Nombre} es superior a la del enemigo {pEnemigo.Nombre}"); }
            else if (pJugador.Vida < pEnemigo.Vida) { Console.WriteLine($"La vida del enemigo {pEnemigo.Nombre} es superior a la del jugador {pJugador.Nombre}"); }
        }

        static void VerMapa(object[,] mapa)
        {
            for (int i = 0; i < mapa.GetLength(0); i++)
            {
                for (int j = 0; j < mapa.GetLength(1); j++)
                {
                    switch (mapa[i, j].ToString())
                    {
                        case "M3UF4Hackatlon_MartinezArmando_RodriguezAdrian.Jugador":
                            Console.Write("\tP");
                            break;
                        case "M3UF4Hackatlon_MartinezArmando_RodriguezAdrian.Enemigo":
                            Console.Write("\tE");
                            break;
                        case "M3UF4Hackatlon_MartinezArmando_RodriguezAdrian.Tesoro":
                            Console.Write("\tT");
                            break;
                        case "M3UF4Hackatlon_MartinezArmando_RodriguezAdrian.Empty":
                            Console.Write("\t-");
                            break;
                    }
                }
                Console.WriteLine();
            }
        }
    }
}
