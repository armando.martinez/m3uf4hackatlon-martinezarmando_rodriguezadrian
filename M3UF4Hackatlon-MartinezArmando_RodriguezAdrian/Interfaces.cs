﻿using System;

namespace M3UF4Hackatlon_MartinezArmando_RodriguezAdrian
{
    public interface IRecolectable
    {
        void Recoger();
    }

    public interface IAtacable
    {
        void RecibirDamage(int damage);
        bool Morir();
        void Atacar(IAtacable obj);
    }
}